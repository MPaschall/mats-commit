﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace YellowGroupCST247.Models

{
    public class GridCell
    {
        [Key]
        public int GridCellID { get; set; }
        public string IsMine { get; set; }
        public bool IsClicked { get; set; }
        public bool IsAdjacent { get; set; }
        public string CellName { get; set; }
        public int CellLocX { get; set; }
        public int CellLocY { get; set; }

    }

    public class GridRow
    {
        [Key]
        public int GridRowID { get; set; }
        public List<GridCell> Cells { get; set; }

    }


    public class GameBoard
    {
        [Key]
        public int GameBoardID { get; set; }
        public List<GridRow> GameBoardRows { get; set; }

    }



}
