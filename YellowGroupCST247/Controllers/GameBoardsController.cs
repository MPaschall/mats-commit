﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using YellowGroupCST247.Models;
using System.Web.Routing;

namespace YellowGroupCST247.Controllers
{
    public class GameBoardsController : ApiController
    {
        private MyDBContext db = new MyDBContext();

        // GET: api/GameBoards
        public IQueryable<GameBoard> GetGameBoards()
        {
            return db.GameBoards;
        }

        // GET: api/GameBoards/5
        [ResponseType(typeof(GameBoard))]
        public async Task<IHttpActionResult> GetGameBoard(int id)
        {
            GameBoard gameBoard = await db.GameBoards.FindAsync(id);
            if (gameBoard == null)
            {
                return NotFound();
            }

            return Ok(gameBoard);
        }

        // PUT: api/GameBoards/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGameBoard(int id, GameBoard gameBoard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != gameBoard.GameBoardID)
            {
                return BadRequest();
            }

            db.Entry(gameBoard).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GameBoardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GameBoards
        [ResponseType(typeof(GameBoard))]
        public async Task<IHttpActionResult> PostGameBoard(GameBoard gameBoard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.GameBoards.Add(gameBoard);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = gameBoard.GameBoardID }, gameBoard);
        }

        // DELETE: api/GameBoards/5
        [ResponseType(typeof(GameBoard))]
        public async Task<IHttpActionResult> DeleteGameBoard(int id)
        {
            GameBoard gameBoard = await db.GameBoards.FindAsync(id);
            if (gameBoard == null)
            {
                return NotFound();
            }

            db.GameBoards.Remove(gameBoard);
            await db.SaveChangesAsync();

            return Ok(gameBoard);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GameBoardExists(int id)
        {
            return db.GameBoards.Count(e => e.GameBoardID == id) > 0;
        }
    }
}