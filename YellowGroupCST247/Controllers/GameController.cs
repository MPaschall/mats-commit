﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using YellowGroupCST247.Models;


namespace YellowGroupCST247.Controllers
{
    public class GameController : Controller
    {


        public ActionResult GameView()

        {
            int bombs = 20;
            int location = 0;
            Button button = new Button();
            button.Height = 30;
            button.Width = 30;
            Random rnd = new Random();
            var bombList = new List<int>();

            for (int b = 0; b < 20; b++)
            {
                int randBomb = rnd.Next(0, 99);
                bombList.Add(randBomb);
            }

            var board = new GameBoard();
            board.GameBoardRows = new List<GridRow>();

            for (var i = 0; i < 10; i++)

            {

                var row = new GridRow(); //each time through the loop create a row
                row.Cells = new List<GridCell>();

                for (var j = 0; j < 10; j++)

                {

                    var cell = new GridCell(); //each time through the loop create a cell


                    if (bombList.Contains((i * 10) + j))

                    {

                        cell.IsMine = "Bomb";
                        cell.CellLocX = getX(location);
                        cell.CellLocY = getY(location);
                        location++;

                    }
                    else
                    {
                        cell.IsMine = "No Bomb";
                        cell.CellLocX = getX(location);
                        cell.CellLocY = getY(location);
                        location++;
                    }

                    row.Cells.Add(cell); //add the cell to the row


                }



                board.GameBoardRows.Add(row); //add the row to the board

            }


            return View(board);


        }

        private int getX(int xLoc)
        {
            int x = 0;
            if (xLoc <= 9)
            {
                x = xLoc;
            }
            else
            {
                x = xLoc % 10;
            }
            return x;
        }

        private int getY(int yLoc)
        {
            int y = 0;
            if (yLoc <= 9)
            {
                y = 0;
            }
            else
            {
                y = yLoc / 10;
            }
            return y;
        }

        public ActionResult Button()
        {
            var button = GetButton();

            return PartialView("_Button", button);


        }

        private Button GetButton()
        {
            var button = new Button();
            button.Height = 30;
            button.Width = 30;
            button.Visible = true;
            button.BorderColor = System.Drawing.Color.AliceBlue;
            button.BackColor = System.Drawing.Color.Aqua;

            return button;

        }

        public ActionResult UpdateBoard()
        {
            var grid = new List<Cell>();
            for (int i = 1; i < 10; i++)
            {
                grid.Add(new Models.Cell { cell = i });
            }
            return PartialView("_Game", grid);
        }

        public ActionResult LoadGame()
        {
            using (MyDBContext db = new MyDBContext())
            {
                var board = new GameBoard();
                /* Select gameboard and load based on userID
                */

                return View(board);
            }

        }

        public ActionResult SaveGame(GameBoard gameBoard)
        {
            using (MyDBContext db = new MyDBContext())
            {
                var board = new GameBoard();
                /* Select gameboard and save based on userID, gotten from saveButtons() on gameview form.
                 
                 */

                return View(board);
            }

        }
    }
}




