﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YellowGroupCST247.Models;

namespace YellowGroupCST247.Repository
{
    public class UserInfoRepository : IUserInfoRepository

    {
        public UserInfoRepository()
        {
            Add(new UserAccount { FirstName = "Test", LastName = "Test", Sex = "Test", Age = 99 });

        }

        private List<UserAccount> UserList = new List<UserAccount>();
        private int _nextId = 1;


        public UserAccount Add(UserAccount item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            // TO DO : Code to save record into database 
            item.UserID = _nextId++;
            UserList.Add(item);
            return item;
            ;
        }

        public bool Delete(int id)
        {
            UserList.RemoveAll(p => p.UserID == id);
            return true;
        }

        public UserAccount Get(int id)
        {
            return UserList.Find(p => p.UserID == id);

        }

        public IEnumerable<UserAccount> GetAll()
        {
            return UserList; ;
        }

        public bool Update(UserAccount item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            // TO DO : Code to update record into database 
            int index = UserList.FindIndex(p => p.UserID == item.UserID);
            if (index == -1)
            {
                return false;
            }
            UserList.RemoveAt(index);
            UserList.Add(item);
            return true;

        }
    }
}