﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using YellowGroupCST247.Models;

namespace YellowGroupCST247.Repository
{
    public interface IUserInfoRepository
    {
        IEnumerable<UserAccount> GetAll();

        UserAccount Get(int id);

        UserAccount Add(UserAccount item);

        bool Update(UserAccount item);

        bool Delete(int id);
    }
}
