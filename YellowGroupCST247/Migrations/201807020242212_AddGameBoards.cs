namespace YellowGroupCST247.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGameBoards : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GameBoards",
                c => new
                    {
                        GameBoardID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.GameBoardID);
            
            CreateTable(
                "dbo.GridRows",
                c => new
                    {
                        GridRowID = c.Int(nullable: false, identity: true),
                        GameBoard_GameBoardID = c.Int(),
                    })
                .PrimaryKey(t => t.GridRowID)
                .ForeignKey("dbo.GameBoards", t => t.GameBoard_GameBoardID)
                .Index(t => t.GameBoard_GameBoardID);
            
            CreateTable(
                "dbo.GridCells",
                c => new
                    {
                        GridCellID = c.Int(nullable: false, identity: true),
                        IsMine = c.String(),
                        IsClicked = c.Boolean(nullable: false),
                        IsAdjacent = c.Boolean(nullable: false),
                        CellName = c.String(),
                        CellLocX = c.Int(nullable: false),
                        CellLocY = c.Int(nullable: false),
                        GridRow_GridRowID = c.Int(),
                    })
                .PrimaryKey(t => t.GridCellID)
                .ForeignKey("dbo.GridRows", t => t.GridRow_GridRowID)
                .Index(t => t.GridRow_GridRowID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GridRows", "GameBoard_GameBoardID", "dbo.GameBoards");
            DropForeignKey("dbo.GridCells", "GridRow_GridRowID", "dbo.GridRows");
            DropIndex("dbo.GridCells", new[] { "GridRow_GridRowID" });
            DropIndex("dbo.GridRows", new[] { "GameBoard_GameBoardID" });
            DropTable("dbo.GridCells");
            DropTable("dbo.GridRows");
            DropTable("dbo.GameBoards");
        }
    }
}
